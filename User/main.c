/********************************** (C) COPYRIGHT *******************************
 * File Name          : main.c
 * Author             : Oleg
 * Version            :
 * Date               :
 * Description        : Main program body.
*********************************************************************************
* Copyright (c)
*******************************************************************************/

#include "ch32v10x.h"
#include "pinmacro.h"
#include "hardware.h"
#include "contiki.h"
#include "dev/watchdog.h"
#include "button.h"
#include "led_blink.h"


/* Global typedef */

/* Global define */

/* Global Variable */

AUTOSTART_PROCESSES(&blink_process,
                    &btnkey_process);

uint32_t idle_count = 0;


void hw_init(void)
{
    RCC->APB2PCENR = RCC_IOPAEN | RCC_IOPBEN;

    GPIO_config(BLUELED);
    GPIO_config(BTNKEY);
}

/*********************************************************************
 * @fn      main
 *
 * @brief   Main program.
 *
 * @return  none
 */
int main(void)
{
    hw_init();

    clock_init();

    process_init();

    process_start(&etimer_process, NULL);

    watchdog_init();

    autostart_start(autostart_processes);

    watchdog_start();

    while(1)
    {
        do {watchdog_periodic();}
        while(process_run()>0);
        idle_count++;
    }
}
