/*
 * button.c
 *
 *  Created on: Apr 10, 2023
 *      Author: Oleg
 */

#include "ch32v10x.h"
#include <stdbool.h>
#include "contiki.h"
#include "pinmacro.h"
#include "hardware.h"


PROCESS(btnkey_process, "ButtonKey");


process_event_t button_press_event;
process_event_t button_release_event;


void button_init()
{
    button_press_event = process_alloc_event();
    button_release_event = process_alloc_event();
}


PROCESS_THREAD(btnkey_process, ev, data)
{
    static struct etimer et;

    PROCESS_BEGIN();

    button_init();

    etimer_set(&et, 30);

    while(1)
    {
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

        static uint32_t button_num;
        static bool KEY_curr_state, KEY_prev_state;

        KEY_curr_state = GPI_ON(BTNKEY); button_num = BUILTIN_BTN;
        if( KEY_curr_state == false && KEY_prev_state == true )
        {
            process_post(PROCESS_BROADCAST, button_release_event, &button_num);
        }
        if( KEY_curr_state == true && KEY_prev_state == false )
        {
            process_post(PROCESS_BROADCAST, button_press_event, &button_num);
        }
        KEY_prev_state = KEY_curr_state;

        etimer_reset(&et);
    }

    PROCESS_END();
}
