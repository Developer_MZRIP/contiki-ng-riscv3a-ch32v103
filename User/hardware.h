/*
 * hardware.h
 *
 *  Created on: Dec 19, 2022
 *      Author: Oleg
 */

#ifndef USER_HARDWARE_H_
#define USER_HARDWARE_H_


#define BLUELED     B,2,1,GPIO_PP2

#define BUILTIN_BTN 0x01U
#define BTNKEY      A,0,1,GPIO_PULL


#endif /* USER_HARDWARE_H_ */
