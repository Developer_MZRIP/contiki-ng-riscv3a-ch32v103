/*
 * button.h
 *
 *  Created on: Apr 10, 2023
 *      Author: Oleg
 */

#ifndef USER_BUTTON_H_
#define USER_BUTTON_H_


extern process_event_t button_press_event;
extern process_event_t button_release_event;

extern struct process btnkey_process;


#endif /* USER_BUTTON_H_ */
