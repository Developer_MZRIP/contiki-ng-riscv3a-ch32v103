/*
 * button.c
 *
 *  Created on: Apr 10, 2023
 *      Author: Oleg
 */

#include "ch32v10x.h"
#include "contiki.h"
#include "pinmacro.h"
#include "hardware.h"
#include "button.h"


PROCESS(blink_process, "Blink");

PROCESS_THREAD(blink_process, ev, data)
{
    static struct etimer et;
    static uint32_t blink_period;

    PROCESS_BEGIN();

    blink_period = CLOCK_SECOND/4;
    etimer_set(&et, blink_period);

    while(1)
    {
        PROCESS_WAIT_EVENT_UNTIL( ev == PROCESS_EVENT_TIMER ||
                                  ev == button_press_event );

        if ( ev == PROCESS_EVENT_TIMER )
        {
            GPO_T(BLUELED);

            etimer_reset(&et);
        }
        else if ( ev == button_press_event &&
                  *(uint32_t *)data == BUILTIN_BTN )
        {
            if ( blink_period == CLOCK_SECOND/4 )
            {
                blink_period = CLOCK_SECOND/8;
            }
            else
            {
                blink_period = CLOCK_SECOND/4;
            }

            etimer_reset_with_new_interval(&et, blink_period);
        }
    }

    PROCESS_END();
}
