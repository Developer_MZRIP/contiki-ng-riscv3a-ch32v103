/*
 *  Copyright (c)
 *  All rights reserved
 *
 * @Header:
 * @Author :
 * @Email  :
 * @Date :
 * @Update:
 *
 */

#ifndef CONTIKI_CONF_H_
#define CONTIKI_CONF_H_

#include <stdint.h>
#include <stddef.h>

#define CLOCK_CONF_SIZE 4
#define CLOCK_CONF_SECOND 1000

#define CCIF
#define CLIF

typedef uint8_t u8_t;
typedef uint16_t u16_t;
typedef uint32_t u32_t;
typedef int8_t s8_t;
typedef int16_t s16_t;
typedef int32_t s32_t;


#endif /* CONTIKI_CONF_H */
