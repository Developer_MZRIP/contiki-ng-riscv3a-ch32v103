/*
 *  Copyright (c)
 *  All rights reserved
 *
 * @Header :
 * @Author :
 * @Email  :
 * @Date :
 * @Update:
 *
 */

#include "ch32v10x.h"
#include <sys/clock.h>
#include <sys/cc.h>
#include <sys/etimer.h>


static volatile clock_time_t current_clock = 0;
static volatile unsigned long current_seconds = 0;
static unsigned int second_countdown = CLOCK_SECOND;


__attribute__((interrupt("WCH-Interrupt-fast")))
void
SysTick_Handler(void)
{
	current_clock++;
	if(etimer_pending() && etimer_next_expiration_time() <= current_clock) {
        etimer_request_poll();
        /* printf("%d,%d\n", clock_time(),etimer_next_expiration_time  	()); */
    }
	if (--second_countdown == 0) {
        current_seconds++;
        second_countdown = CLOCK_SECOND;
    }

    SysTick->CTLR = 0;
    SYSTICK_REG_WRITE8(CNTL, 0);
    SYSTICK_REG_WRITE8(CNTH, 0);
    SysTick->CTLR = SYSTICK_CTLR_STE;
}

void
clock_init()
{
    SysTick->CTLR = 0;
    SYSTICK_REG_WRITE8(CNTL, 0);
    SYSTICK_REG_WRITE8(CNTH, 0);
    SYSTICK_REG_WRITE8(CMPLR, (SystemCoreClock/8/CLOCK_SECOND-1));
    SYSTICK_REG_WRITE8(CMPHR, 0);
    SysTick->CTLR = SYSTICK_CTLR_STE;

    NVIC_SetPriority(SysTicK_IRQn, 0xf0);
    NVIC_EnableIRQ(SysTicK_IRQn);
}

clock_time_t
clock_time(void)
{
  return current_clock;
}

unsigned long
clock_seconds(void)
{
  return current_seconds;
}
